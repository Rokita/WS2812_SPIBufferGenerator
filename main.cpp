#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#define RETURN_FILE_PROBLEM (-1)
#define LOW_LEVEL 0x03
#define HIGH_LEVEL 0x7C

using namespace std;

fstream fInputFile;
fstream fOutputFile_h;
fstream fOutputFile_c;
string FileName_h;
string FileName_c;
string FileName;



typedef struct ColorTheme
{
    string name;
    int *LedColorsTab;
} ColorTheme_T;

typedef struct LedsParameters
{
    int NumberOfLeds;
    int ColorsPerLed;
    int BitsPerColor;
    int BytesPerTheme;
    int ThemeBufferSize;
} LedsParameters_T;

int VALUE_TAB[2] = {LOW_LEVEL, HIGH_LEVEL};


void initializeFiles();
void readLedsParametersFromInputFile(LedsParameters_T *pkLedsParameters);
string readColorName();
void readColorTab(LedsParameters_T *pkLedsParameters, int *ColorBuffer);
void converColorTabToThemeBuff(LedsParameters_T *pkLedsParameters, int *ColorBuff, int *OutputThemeBuff);
void generateOutputFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter);
void generateHeaderFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter);
void generateSourceFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter);

void closeFiles();

int main()
{

    initializeFiles();
    LedsParameters_T kLedsParameters;
    readLedsParametersFromInputFile(&kLedsParameters);

    int *ColorBuff = new int[kLedsParameters.BytesPerTheme];
    ColorTheme_T AllColorThemes[100];
    int counter = 0;

    while (fInputFile.eof() == false)
    {
        AllColorThemes[counter].name = readColorName();
        string str = "  ";
        if(true == AllColorThemes[counter].name.empty()) // je�li nic dalej w pliku nie ma to przestan wczytywac
        {
            break;
        }
        cout << AllColorThemes[counter].name << endl;
        AllColorThemes[counter].LedColorsTab = new int[kLedsParameters.ThemeBufferSize];
        readColorTab(&kLedsParameters, ColorBuff);
        converColorTabToThemeBuff(&kLedsParameters, ColorBuff, AllColorThemes[counter].LedColorsTab );
        counter++;
    }

    cout << "Output file generation... " <<endl;
    generateOutputFiles(&kLedsParameters, AllColorThemes, counter);
    cout << "Generation Finished - OK!" << endl;

    for(int iter = 0; iter < counter; ++iter)
    {
        delete [] AllColorThemes[iter].LedColorsTab;
    }
    delete [] ColorBuff;
    closeFiles();
    cin.get();
    return 0;
}




void initializeFiles()
{
    cout << "Colors file name: ";
    cin >> FileName;
    fInputFile.open(FileName.c_str(), ios::in);
    if(fInputFile.is_open() == false)
    {
        cout << endl << "Input file available - FAIL!" << endl;
        cout << endl << "================================"<<endl;
        cout << endl << "Process canceled!" << endl;
        exit(RETURN_FILE_PROBLEM);
    }
    cout << "Input file available - OK!" << endl << endl;
    cout << "Output file name: ";
    cin >> FileName;
    FileName_h = FileName + ".h";
    FileName_c = FileName + ".c";
    fOutputFile_h.open(FileName_h.c_str(), ios::out);
    if(fOutputFile_h.is_open() == false)
    {
        cout << endl << "Output file *.h available - FAIL!" << endl;
        cout << endl << "================================"<<endl;
        cout << endl << "Process canceled!" << endl;
        exit(RETURN_FILE_PROBLEM);
    }

    cout << "Output file *.h available - OK!" << endl << endl;

    fOutputFile_c.open(FileName_c.c_str(), ios::out);
    if(fOutputFile_c.is_open() == false)
    {
        cout << endl << "Output file *.c available - FAIL!" << endl;
        cout << endl << "================================"<<endl;
        cout << endl << "Process canceled!" << endl;
        exit(RETURN_FILE_PROBLEM);
    }
    cout << "Output file *.c available - OK!" << endl << endl;

    return;
}


void readLedsParametersFromInputFile(LedsParameters_T *pkLedsParameters)
{
    fInputFile >> pkLedsParameters->NumberOfLeds;
    fInputFile >> pkLedsParameters->ColorsPerLed;
    fInputFile >> pkLedsParameters->BitsPerColor;
    pkLedsParameters->BytesPerTheme = pkLedsParameters->NumberOfLeds * pkLedsParameters->ColorsPerLed;
    pkLedsParameters->ThemeBufferSize = pkLedsParameters->BytesPerTheme * pkLedsParameters->BitsPerColor;
    return;
}

string readColorName()
{
    string ColorName;
    fInputFile >> ColorName;
    return ColorName;
}

void readColorTab(LedsParameters_T *pkLedsParameters, int *ColorBuffer)
{
    for(int iterator = 0; iterator < pkLedsParameters->BytesPerTheme; ++iterator)
    {
        fInputFile >> hex >> ColorBuffer[iterator];
    }
    return;
}


void converColorTabToThemeBuff(LedsParameters_T *pkLedsParameters, int *ColorBuff, int *OutputThemeBuff)
{
    for (int Iter=0; Iter< pkLedsParameters->BytesPerTheme; ++Iter)
    {
        for (int iterator = 0; iterator < pkLedsParameters->BitsPerColor; ++iterator)
        {
            int BitIndex = pkLedsParameters->BitsPerColor-iterator -1;
            int value = (ColorBuff[Iter] >> BitIndex)& 0x01;
            int ByteIndex = Iter * pkLedsParameters->BitsPerColor + iterator;
            OutputThemeBuff[ByteIndex] = VALUE_TAB[value];
        }
    }
    return;
}

void generateOutputFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter)
{
    generateHeaderFiles(pkLedsParameters, AllColorThemes, counter);
    generateSourceFiles(pkLedsParameters, AllColorThemes, counter);
}


void generateHeaderFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter)
{
    fOutputFile_h << "#ifndef " << FileName << "_H" << endl;
    fOutputFile_h << "#define " << FileName << "_H" << endl << endl;

    fOutputFile_h << "#include <stdint.h>" <<endl << endl;

    fOutputFile_h << "#define NUMBER_OF_THEMES (" << counter << ")" << endl;
    fOutputFile_h << "#define NUMBER_OF_LEDS (" << pkLedsParameters->NumberOfLeds << ")" << endl;
    fOutputFile_h << "#define COLORS_PER_LED (" << pkLedsParameters->ColorsPerLed << ")" << endl;
    fOutputFile_h << "#define BITS_PER_COLOR (" << pkLedsParameters->BitsPerColor << ")" << endl;
    fOutputFile_h << "#define BYTES_PER_THEME ((NUMBER_OF_LEDS)*(COLORS_PER_LED))" << endl;
    fOutputFile_h << "#define THEME_BUFFER_SIZE ((BYTES_PER_THEME)*(BITS_PER_COLOR)) "<< endl << endl;

    fOutputFile_h << "typedef enum WS2812Theme" << endl ;
    fOutputFile_h << "   {" << endl;
    for (int iter = 0; iter < counter; ++iter)
    {
        fOutputFile_h << "   eTheme_" << AllColorThemes[iter].name << "," << endl;
    }
    fOutputFile_h << "   eTheme_NumOf"  << endl;
    fOutputFile_h << "   }WS2812Theme_T;" << endl << endl;

    fOutputFile_h << "extern uint8_t *WS2812_ColorsTab[];" << endl << endl;

    fOutputFile_h << endl << "#endif" << endl;
    return;
}

void generateSourceFiles(LedsParameters_T *pkLedsParameters, ColorTheme_T * AllColorThemes, int counter)
{
    fOutputFile_c << "#include \"" << FileName_h << "\"" << endl << endl;
    for(int iter = 0 ; iter < counter; iter ++)
    {
        fOutputFile_c << "uint8_t ThemeTab_" << AllColorThemes[iter].name << "[THEME_BUFFER_SIZE] =" << endl << "   {";
        for (int iterator = 0; iterator < pkLedsParameters->ThemeBufferSize - 1; ++iterator) //without last value. It will be added after loop
        {
            if(iterator % 8 == 0)
            {
                fOutputFile_c<<endl<<"   ";
            }
            fOutputFile_c << AllColorThemes[iter].LedColorsTab[iterator] << ", ";
        }
        fOutputFile_c << AllColorThemes[iter].LedColorsTab[pkLedsParameters->ThemeBufferSize-1] << endl;
        fOutputFile_c << "   };" << endl << endl;
    }


    fOutputFile_c << "uint8_t *WS2812_ColorsTab[NUMBER_OF_THEMES] =" << endl ;
    fOutputFile_c << "   {" << endl;
    for (int iter = 0; iter < counter; ++iter)
    {
        fOutputFile_c << "   ThemeTab_" << AllColorThemes[iter].name << "," << endl;
    }
    fOutputFile_c << "   };" << endl << endl;

    return;
}

void closeFiles()
{
    fInputFile.close();
    fOutputFile_h.close();
    fOutputFile_c.close();
}


