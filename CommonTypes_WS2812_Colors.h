#ifndef Commontypes_WS2812_Colors_H
#define Commontypes_WS2812_Colors_H

#include <stdint.h>

#define NUMBER_OF_THEMES (8)
#define NUMBER_OF_LEDS (16)
#define COLORS_PER_LED (3)
#define BITS_PER_COLOR (8)
#define BYTES_PER_THEME ((NUMBER_OF_LEDS)*(COLORS_PER_LED))
#define THEME_BUFFER_SIZE ((BYTES_PER_THEME)*(BITS_PER_COLOR)) 

typedef enum WS2812Theme
   {
   eTheme_White,
   eTheme_WhiteAndRed,
   eTheme_GreenAndYellow,
   eTheme_Blue,
   eTheme_Purple,
   eTheme_OrangeAndPink,
   eTheme_RedAndBlue,
   eTheme_NavyBlue,
   eTheme_NumOf
   }WS2812Theme_T;

extern uint8_t *WS2812_ColorsTab[];


#endif
