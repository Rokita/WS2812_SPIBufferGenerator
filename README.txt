#README 
#This file describe how input file should looks like. 

#Input file should consist as follow

## Number of Leds WS2812 (ex. 2 for 2 leds)
## Number of each Led colors (ex. 3 for RGB)
## Number of bits for one color (ex. 8 bits for one color)

## First color theme name (ex. White)
## Hexadecimal values of all leds colors separated by white sigh (space or enter)
## (ex. for previous parameters 2 leds with 3 colors 
## (grb - standard WS2812)
## 0xFF 0xFF 0xFF
## 0xFF
## 0xFF 0xFF 

## Second color theme name with followed values 

## and so on. 
